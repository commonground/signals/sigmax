FROM python:3.7-alpine

RUN apk --no-cache add gcc musl-dev linux-headers

WORKDIR /app
COPY requirements.txt /app/
RUN /usr/local/bin/pip install --no-cache-dir --requirement requirements.txt

COPY app.py /app/

ENTRYPOINT ["/usr/local/bin/uwsgi"]

CMD ["--http", ":8080", "--manage-script-name", "--wsgi-file", "/app/app.py", "--callable", "app"]
