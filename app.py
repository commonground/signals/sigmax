from flask import Flask, Response, request


app = Flask(__name__)


STUF_RESPONSE = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><Lk01Response xmlns="http://tempuri.org/"><Bv03Bericht xmlns="http://www.egem.nl/StUF/StUF0301"><stuurgegevens><berichtcode>Bv03</berichtcode><zender><organisatie/><applicatie/></zender><ontvanger><organisatie/><applicatie/></ontvanger><referentienummer>1c932a8c-d3cd-4083-bffa-7f29ac22a5ac</referentienummer><tijdstipBericht>20181002153711947</tijdstipBericht></stuurgegevens></Bv03Bericht></Lk01Response></s:Body></s:Envelope>'


@app.route("/", defaults={"path": ""}, methods=["GET", "POST"])
@app.route("/<path:path>")
def index(path):
    if request.method == "POST":
        return Response(STUF_RESPONSE,  mimetype="text/xml")

    return ""


if __name__ == "__main__":
    app.run(debug=True)
